/*
 * @Copyright 2019 VOID SOFTWARE, S.A.
 */
package com.test.boundary;

import com.test.entity.TestEntity;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/test")
@RequestScoped
@Tag(name = "Test")
public class TestResource {

    @Inject
    TestEntityRepository testEntityRepository;

    @POST
    @Path("/test")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create")
    @APIResponse(content = @Content(schema = @Schema(implementation = TestEntity.class, type = SchemaType.OBJECT)))
    public Response login() {
        return Response.ok(testEntityRepository.store(new TestEntity("fieldToBeChanged"))).build();
    }

    @POST
    @Path("/test/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Edit")
    @APIResponse(content = @Content(schema = @Schema(implementation = TestEntity.class, type = SchemaType.OBJECT)))
    public Response login(@NotNull @Min(1) @PathParam("id") long id) {
        return Response.ok(testEntityRepository.edit(id)).build();
    }

}
