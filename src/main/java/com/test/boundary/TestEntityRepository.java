/*
 *
 * @Copyright 2019 VOID SOFTWARE, S.A.
 *
 */
package com.test.boundary;

import com.test.entity.TestEntity;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
public class TestEntityRepository extends PersistenceRepository<TestEntity> {

    @Transactional
    public TestEntity edit(long id) {
        TestEntity testEntity = getNotNull(id);
        testEntity.changeValue();
        return testEntity;
    }
}
