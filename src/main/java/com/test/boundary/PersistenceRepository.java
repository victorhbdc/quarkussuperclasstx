/*
 * @Copyright 2019 VOID SOFTWARE, S.A.
 */
package com.test.boundary;


import com.test.entity.BaseEntity;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


public abstract class PersistenceRepository<T extends BaseEntity> {
    protected Class<T> type;

    @Inject
    EntityManager em;

    @SuppressWarnings("unchecked")
    protected PersistenceRepository() {
        Class<? extends PersistenceRepository> aClass = getClass();
        while (!(aClass.getGenericSuperclass() instanceof ParameterizedType)) {
            aClass = (Class<? extends PersistenceRepository>) aClass.getSuperclass();
        }

        Type paramType = ((ParameterizedType) aClass.getGenericSuperclass()).getActualTypeArguments()[0];
        try {
            this.type = (Class<T>) Class.forName(paramType.getTypeName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected EntityManager getManager() {
        return em;
    }

    private Class<T> getType() {
        return type;
    }

    @Transactional
    public T get(long id) {
        return this.em.find(getType(), id);
    }

    @Transactional
    public T getNotNull(long id) {
        T t = get(id);
        if (t == null) {
           throw new RuntimeException();
        }
        return t;
    }

    @Transactional
    public T store(T entity) {
        return getManager().merge(entity);
    }
}
