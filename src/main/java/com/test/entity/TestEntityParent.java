/*
 *
 * @Copyright 2019 VOID SOFTWARE, S.A.
 *
 */
package com.test.entity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class TestEntityParent extends BaseEntity {

    protected String testString;

    public TestEntityParent() {
    }

    public TestEntityParent(String testString) {
        this.testString = testString;
    }

    public String getTestString() {
        return testString;
    }
}
