/*
 *
 * @Copyright 2019 VOID SOFTWARE, S.A.
 *
 */
package com.test.entity;

import javax.persistence.Entity;

@Entity
public class TestEntity extends TestEntityParent {


    public TestEntity() {
    }

    public TestEntity(String testString) {
        super(testString);
    }

    public void changeValue() {
        this.testString = "thisStringShouldBePersisted";
    }
}
