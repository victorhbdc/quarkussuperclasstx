/*
 * @Copyright 2019 VOID SOFTWARE, S.A.
 */
package com.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseEntity implements Serializable {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @SequenceGenerator(name = "test_sequence", sequenceName = "test_id_sequence")
    @GeneratedValue(generator = "test_sequence", strategy = GenerationType.SEQUENCE)
    private long id;

    public BaseEntity() {
    }

    public long getId() {
        return id;
    }

    @JsonIgnore
    public boolean isNew() {
        return this.id <= 0;
    }
}
